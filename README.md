![](https://img.shields.io/badge/-CONFIDENTIAL-red)

**Robocon 2022 Rulebook**: [Game_Rules](https://bitbucket.org/Suyash__Mali/robocon-2022/src/Game_Rules/)

**OneDrive**: [click here](https://fcrit-my.sharepoint.com/personal/mali_suyash_fcrit_onmicrosoft_com/_layouts/15/onedrive.aspx?id=/personal/mali_suyash_fcrit_onmicrosoft_com/Documents/Robocon%202022&FolderCTID=0x012000225C9CE5F7F18E468A476B1DA81D3896){:target="_blank"}

**BOHH IP/Automation branch**: [ashvnv](https://bitbucket.org/Suyash__Mali/robocon-2022/src/ashvnv/)

---

### To clone a branch locally
***git clone -b <branch_name> https://bitbucket.org/Suyash__Mali/robocon-2022.git <branch_name>***

### To update the repo files (changes were made locally)
1. Open the terminal from the cloned branch
2. ***git add .*** or ***git add <filename\>*** or ***git add <foldername\>***
3. ***git config user.name <bitbucket_username>***<br>
   ***git config user.email <bitbucket_email>***
4. ***git commit -m <message string\>***
5. ***git push origin <branch_name\>***

"**git add .**" *updates all the files*<br>
*Step 3 can be skipped next time updating the same branch*

### To update the local files (changes were made remotely)
***git pull***